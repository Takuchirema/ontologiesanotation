
import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.StringWriter;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Set;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

import org.semanticweb.owlapi.apibinding.OWLManager;
import org.semanticweb.owlapi.model.*;
import org.semanticweb.owlapi.profiles.OWL2DLProfile;
import org.semanticweb.owlapi.profiles.OWLProfileReport;
import org.semanticweb.owlapi.profiles.OWLProfileViolation;
import org.semanticweb.owlapi.reasoner.NodeSet;
import org.semanticweb.owlapi.reasoner.OWLReasoner;
import org.semanticweb.owlapi.util.OWLOntologyWalker;
import org.semanticweb.owlapi.util.OWLOntologyWalkerVisitorEx;

import javax.xml.parsers.*;
import javax.xml.transform.*;
import javax.xml.transform.dom.*;
import javax.xml.transform.stream.*;

import org.xml.sax.*;
import org.w3c.dom.*;

public class Test {
	
	private static String ontology_path="C:/Users/User/Desktop/computer_science/VacWork/June2016/ontologies/ZuluAfricanWildlifeRDF.owl";
	private static String resources_path="C:/Users/User/Desktop/computer_science/VacWork/June2016/resources";
	
	private static HashMap<String,String> savedNouns=new HashMap<String,String>();
	
	private static HashMap<Character,ArrayList<String>> verbs = new HashMap<Character,ArrayList<String> >();
	
	private static ArrayList<String> adjectives = new ArrayList<String>();
	
	private static int id =0;
	
	private static Document doc;
	private static String xml ="";
	private String defaultXML="C:/Users/User/Desktop/computer_science/VacWork/June2016/resources";
	
	static // Make an  instance of the DocumentBuilderFactory
    DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
	
	public static void main (String[] args) throws Exception{
		
		OWLOntology o = loadOntology();
		
		loadXMLDocument();
		
		//loadNounClasses();
		
		//showOntologyClasses(o);
		
		//walkOntology(o);
		
		//restrictionVisitor(o);
		
		analyseAxioms(o);
		
	}
	
	private static void loadXMLDocument() throws Exception{
		// use the factory to take an instance of the document builder
        DocumentBuilder db = dbf.newDocumentBuilder();
        // parse using the builder to get the DOM mapping of the XML file
        
        try{
        	doc = db.parse(xml);
        }catch (Exception ex){
        	doc = db.newDocument();
        }
        
        // create the root element
        Element rootEle = doc.createElement("roles");
        doc.appendChild(rootEle);
	}
	
	public static String getElementId(){
		id++;
		return ""+id;
	}
	
	public static void incElementId(){
		id++;
	}
	
	public static Document getDocument(){
		return doc;
	}
	
	/** This methods loads all saved verbs into the ArrayList verbs */
	public static HashMap<Character,ArrayList<String>> loadVerbs() throws IOException{
		
		BufferedReader bf = new BufferedReader(new FileReader(resources_path+"/Verbs.txt"));
		
		String line;
		
		char letter = 'z';
		while ( (line = bf.readLine()) != null){
			
			if (line.charAt(0)==letter){
				verbs.get(letter).add(line);
			}else{
				letter = line.charAt(0);
				ArrayList<String> verbList = new ArrayList<String>();
				verbList.add(line);
				verbs.put(letter,verbList);
			}
			//System.out.println("verb: "+line);
		}
		
		return verbs;
	}
	
	/** This methods loads all saved nouns into the HashMap savedNouns
	 * The key is the noun
	 * The value is the noun class */
	public static HashMap<String,String> loadSavedNouns() throws IOException{
		
		BufferedReader bf = new BufferedReader(new FileReader(resources_path+"/Nouns.txt"));
		
		//the first line has the headings
		bf.readLine();
		
		String line;
		
		while ( (line = bf.readLine()) != null){
			
			String[] nc = line.split(",");
			//System.out.println("1st Item: "+nc[0]+Arrays.toString(nc));
			
			savedNouns.put(nc[0],nc[1]);
		}
		
		return savedNouns;
		
	}
	
	/** This methods loads all adjectives into the ArrayList adjectives */
	public static ArrayList<String> loadAdjectives() throws IOException{
		
		BufferedReader bf = new BufferedReader(new FileReader(resources_path+"/Adjectives.txt"));
		
		String line;
		
		while ( (line = bf.readLine()) != null){
			
			adjectives.add(line);
		}
		
		return adjectives;
		
	}
	
	public static OWLOntology loadOntology() throws OWLOntologyCreationException{
		File file = new File(ontology_path);
		
		OWLOntologyManager m = OWLManager.createOWLOntologyManager();
		
		OWLOntology o = m.loadOntologyFromOntologyDocument(file);
		
		return o;
	}
	
	public static void analyseAxioms(OWLOntology o) throws Exception{
    	// Iterate over the axioms of the ontology. There are more than just the subclass
        // axiom, because the class declarations are also axioms.  All in all, there are
        // four:  the subclass axiom and three declarations of named classes.
        System.out.println( "== All Axioms: ==" );
        for ( final OWLAxiom axiom : o.getAxioms() ) {
            System.out.println("#########################\n"+axiom);
            //System.out.println( axiom );
            String currentAxiom = axiom.toString();
            
            AxiomAnalyser a = new AxiomAnalyser(currentAxiom,"");
            a.analyse("",currentAxiom);
            
            System.out.println("--------- document ----------");
            printDocument();
            System.out.println(" ----------------------------");
             
        }
    }
	
	public static void printDocument() throws Exception{
		// write the content into xml file
		TransformerFactory transformerFactory = TransformerFactory.newInstance();
		Transformer transformer = transformerFactory.newTransformer();
		
		transformer.setOutputProperty(OutputKeys.INDENT, "yes");
		transformer.setOutputProperty("{http://xml.apache.org/xslt}indent-amount", "2");
		
		DOMSource source = new DOMSource(doc);
		// Output to console for testing
		StreamResult result = new StreamResult(new StringWriter());
		transformer.transform(source, result);
		
		String xmlString = result.getWriter().toString();
		System.out.println(xmlString);
	}
	
	public static void walkOntology(OWLOntology o){
		OWLOntologyWalker walker = new OWLOntologyWalker(Collections.singleton(o));
		
		OWLOntologyWalkerVisitorEx<Object> visitor = new OWLOntologyWalkerVisitorEx<Object>(walker) {

            @Override
            public Object visit(OWLObjectSomeValuesFrom desc) {
                // Print out the restriction
                System.out.println("\n###"+desc);
                // Print out the axiom where the restriction is used
                String currentAxiom = getCurrentAxiom().toString();
                 
                return "";
            }
        };
        // Now ask the walker to walk over the ontology structure using our
        // visitor instance.
        walker.walkStructure(visitor);
	}
	
	public static void showOntologyClasses(OWLOntology o){
		
		OWL2DLProfile profile = new OWL2DLProfile();
		OWLProfileReport report = profile.checkOntology(o);
		
		System.out.println("got here!!");
		
		for (OWLClass cls : o.getClassesInSignature()){
			System.out.println(cls);
		}
	}

}

