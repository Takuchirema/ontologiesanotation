import java.util.ArrayList;

import javax.xml.parsers.*;
import javax.xml.transform.*;
import javax.xml.transform.dom.*;
import javax.xml.transform.stream.*;

import org.xml.sax.*;
import org.w3c.dom.*;

public class Entity {
	
	private String name="";
	private String id="";
	private String gender="";
	private String nounClass="";
	private String grammNumber="";
	private Element e;
	
	public Entity(String name){
		this.name=name;
	}
	
	public Document createEntity() throws Exception{
		//get the root element
		id=Test.getElementId();
		
		Document doc=Test.getDocument();
        Element rootElement = doc.getDocumentElement();
        
        e=doc.createElement("entity");
        e.setAttribute("name", name);
        e.setAttribute("id", id);
        e.setAttribute("nounClass", nounClass);
        e.setAttribute("grammNumber", grammNumber);
        e.setAttribute("gender", gender);
        
        rootElement.appendChild(e);
		
        return doc;
	}
	
	public void createSubElement(Element subElement){
		e.appendChild(subElement);
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getGender() {
		return gender;
	}

	public void setGender(String gender) {
		this.gender = gender;
	}

	public String getNounClass() {
		return nounClass;
	}

	public void setNounClass(String nounClass) {
		this.nounClass = nounClass;
	}

	public String getGrammNumber() {
		return grammNumber;
	}

	public void setGrammNumber(String grammNumber) {
		this.grammNumber = grammNumber;
	}
	
	public Element getE() {
		return e;
	}

	public void setE(Element e) {
		this.e = e;
	}

}
