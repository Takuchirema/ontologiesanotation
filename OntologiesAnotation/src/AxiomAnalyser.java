import java.io.File;
import java.io.IOException;
import java.io.StringWriter;
import java.util.ArrayList;

import javax.xml.parsers.*;
import javax.xml.transform.*;
import javax.xml.transform.dom.*;
import javax.xml.transform.stream.*;
import org.xml.sax.*;
import org.w3c.dom.*;


public class AxiomAnalyser {
	
	String theAxiom;
	private Document doc;
    
	public AxiomAnalyser(String axiom,String xml) throws Exception{
		this.theAxiom=axiom;
		doc=Test.getDocument();
	}
	
	public Element analyse(String subject,String axiom) throws Exception{
		Element e = null;
		
		String axiomType= axiom.substring(0,axiom.indexOf("(") );
		//system.out.println("axiom type: "+axiomType);
		String innerAxiom = axiom.substring(axiom.indexOf("(")+1,axiom.lastIndexOf(")"));
		//system.out.println("inner axiom: "+innerAxiom);
		
		if (axiomType.equals("SubClassOf")){
			subClassOf(innerAxiom,"SubClassOf");
		}else if (axiomType.equals("EquivalentClasses")){
			subClassOf(innerAxiom,"EquivalentClasses");
		}else if (axiomType.equals("ObjectSomeValuesFrom")){
			e = objectSomeValuesFrom(subject,innerAxiom,"existential");
		}else if (axiomType.equals("ObjectAllValuesFrom")){
			e = objectSomeValuesFrom(subject,innerAxiom,"universal");
		}else if (axiomType.equals("ObjectIntersectionOf")){
			e = objectIntersectionOf(subject,innerAxiom,"ObjectIntersectionOf");
		}else if (axiomType.equals("ObjectUnionOf")){
			e = objectIntersectionOf(subject,innerAxiom,"ObjectUnionOf");
		}else if (axiomType.equals("ObjectComplementOf")){
			e = objectIntersectionOf(subject,innerAxiom,"ObjectComplementOf");
		}
		
		return e;
	}
	
	public Element analyse(String subject,String axiomType,String innerAxiom) throws Exception{
		Element e = null;
		
		if (axiomType.equals("SubClassOf")){
			subClassOf(innerAxiom,"SubClassOf");
		}else if (axiomType.equals("EquivalentClasses")){
			subClassOf(innerAxiom,"EquivalentClasses");
		}else if (axiomType.equals("ObjectSomeValuesFrom")){
			e = objectSomeValuesFrom(subject,innerAxiom,"existential");
		}else if (axiomType.equals("ObjectAllValuesFrom")){
			e = objectSomeValuesFrom(subject,innerAxiom,"universal");
		}else if (axiomType.equals("ObjectIntersectionOf")){
			e = objectIntersectionOf(subject,innerAxiom,"ObjectIntersectionOf");
		}else if (axiomType.equals("ObjectUnionOf")){
			e = objectIntersectionOf(subject,innerAxiom,"ObjectUnionOf");
		}else if (axiomType.equals("ObjectComplementOf")){
			e = objectIntersectionOf(subject,innerAxiom,"ObjectComplementOf");
		}
		
		return e;
	}

	public void subClassOf(String values,String tag) throws Exception{
		
		Element e=null;
		
		String subject = values.substring(0,values.indexOf(" "));
		//system.out.println("subject: "+subject);
		String subjectClass = subject.substring(subject.indexOf("#")+1,subject.length()-1);
		//system.out.println("subjectClass: "+subjectClass);
		
		String axiom=values.substring(values.indexOf(" ")+1,values.length());
		
		Node subjectEntityNode = elementExists(subjectClass,doc,"entity");
		
		if ( subjectEntityNode==null) {
            Entity subjectEntity = new Entity(subjectClass);
            doc = subjectEntity.createEntity();
            
            e=subjectEntity.getE();
    		//printDocument();
        }else{
        	System.out.println("Entity not null! "+subjectClass);
        	e=(Element)subjectEntityNode;
        }
		
		String axiomType="";
		if (axiom.charAt(0)!='<'){
			axiomType= axiom.substring(0,axiom.indexOf("(") );
			
			System.out.println("axiom type: "+axiomType+" char 0: "+axiom.charAt(0));
			String innerAxiom = axiom.substring(axiom.indexOf("(")+1,axiom.lastIndexOf(")"));
			System.out.println("inner axiom: "+innerAxiom);
			
			String subjectId = e.getAttributes().getNamedItem("id").getNodeValue();
			
			Element subElement=analyse(subjectId,axiomType,innerAxiom);
			
			//set the reference for that element
			e.appendChild(subElement);
			
		}else{
			subjectClass = axiom.substring(axiom.indexOf("#")+1,axiom.length()-1);
			
			if (elementExists(subjectClass,doc,"entity")==null) {
		        
		        Element subElement=doc.createElement(tag);
		        subElement.setAttribute("name", subjectClass);
		        subElement.setAttribute("id", Test.getElementId());
		        //Test.incElementId();
		        
		        if (e!=null)
		        	e.appendChild(subElement);
		        
	    		//printDocument();
	        }else{
	        	System.out.println("Entity 2 not null! "+subjectClass);
	        }
		}
		
	}
	
	public Element objectSomeValuesFrom(String subject,String values,String type) throws Exception{
		
		Element e=doc.createElement("RelationshipRef");
		
		String relation = values.substring(0,values.indexOf(" "));
		System.out.println("subject: "+relation);
		String relationClass = relation.substring(relation.indexOf("#")+1,relation.length()-1);
		System.out.println("subjectClass: "+relationClass);
		
		String axiom=values.substring(values.indexOf(" ")+1,values.length());
		
		Node relationship = elementExists(relationClass,doc,"Relationship");
		/* prepare the words and pattern values here */
		if ( relationship == null) {
            Relationship rel = new Relationship(relationClass,type);
            rel.createEntity(doc);
            
            e.setAttribute("ref", rel.getId());
            e.setAttribute("refName", rel.getName());
        }else{
        	String id = relationship.getAttributes().getNamedItem("id").getNodeValue();
        	String name = relationship.getAttributes().getNamedItem("name").getNodeValue();
        	
        	System.out.println("Entity not null: "+relationClass+" Id: "+id);
        	
        	e.setAttribute("ref", id);
        	e.setAttribute("refName", name);
        }
		
		/* create element to give to upper class */
		String axiomType="";
		if (axiom.charAt(0)!='<'){
			axiomType= axiom.substring(0,axiom.indexOf("(") );
			
			System.out.println("axiom type: "+axiomType);
			String innerAxiom = axiom.substring(axiom.indexOf("(")+1,axiom.lastIndexOf(")"));
			System.out.println("inner axiom: "+innerAxiom);
			
			Element r1=doc.createElement("role");
	        r1.setAttribute("ref", subject);
	        r1.setAttribute("name", "r1");
	        
	        Element r2=doc.createElement("role");
			Element rElement=analyse("?",axiomType,innerAxiom);
			r2.setAttribute("ref", "?");
		    r2.setAttribute("name", "r2");
		    r2.appendChild(rElement);
		        
		    e.appendChild(r1);
		    e.appendChild(r2);
			
		}else{
			String objectClass = axiom.substring(axiom.indexOf("#")+1,axiom.length()-1);
			//system.out.println("subjectClass2: "+subjectClass);
			
			//create the roles
	        Element r1=doc.createElement("role");
	        r1.setAttribute("ref", subject);
	        r1.setAttribute("name", "r1");
	        
	        Element r2=doc.createElement("role");
	        r2.setAttribute("name", "r2");
	        
			Node objectEntityNode = elementExists(objectClass,doc,"entity");
			if ( objectEntityNode==null) {
	            Entity subjectEntity = new Entity(objectClass);
	            doc = subjectEntity.createEntity();
	            
	            r2.setAttribute("ref", subjectEntity.getId());
	            r2.setAttribute("refName", subjectEntity.getName());
	    		//printDocument();
	        }else{
	        	String id = objectEntityNode.getAttributes().getNamedItem("id").getNodeValue();
	        	String name = objectEntityNode.getAttributes().getNamedItem("name").getNodeValue();
	        	
	        	System.out.println("Entity not null: "+objectClass+" Id: "+id);
	        	
	        	r2.setAttribute("ref", id);
	        	r2.setAttribute("refName", name);
	        }

	        e.appendChild(r1);
	        e.appendChild(r2);
			
		}
		
		return e;
	}
	
	public String checkAxioms(String subject,Element e,String values) throws Exception{
		
		String axiom2="";
		
		int bracketCount = 0;
		
		loop:
		for (int i=0;i<values.length();i++){
			if (values.charAt(i)=='(')
				bracketCount++;
			else if (values.charAt(i)==')'){
				bracketCount--;
				
				if (bracketCount ==0){
					String axiom1="";
					if (values.length()>=i){
						axiom1 = values.substring(0,i+1);
						System.out.println("**axiom: "+axiom1);
					}else{
						axiom1 = values.substring(0,i);
						System.out.println("##axiom: "+axiom1);
					}
					
					Element subElement=analyse(subject,axiom1);
					e.appendChild(subElement);
					
					axiom2 = values.replace(axiom1, "");
					axiom2 = axiom2.replaceAll("^\\s+", "");
					
					//system.out.println("Replaced axiom: "+axiom2);
					
					break loop;
				}
			}
		}
		
		return axiom2;
	}
	
	public Element objectIntersectionOf(String subject,String values,String tag) throws Exception{
		
		Element e=doc.createElement(tag);
		e.setAttribute("type", "");
		
		//check if the intersections are classes or axioms
		char check = values.charAt(values.charAt(0));
		//system.out.println("check char: "+check);
		
		if (check == '<'){
			String entity = values.substring(0,values.indexOf(" "));
			//system.out.println("subject: "+subject);
			String subjectClass = entity.substring(entity.indexOf("#")+1,entity.length()-1);
			//system.out.println("subjectClass: "+subjectClass);
			
			String axiom=values.substring(values.indexOf(" ")+1,values.length());
			
			Node subjectEntityNode = elementExists(subjectClass,doc,"entity");
			Element entityRef=doc.createElement("EntityRef");
			
			if ( subjectEntityNode==null) {
	            Entity subjectEntity = new Entity(subjectClass);
	            doc = subjectEntity.createEntity();
	            
	            entityRef.setAttribute("ref", subjectEntity.getId());
	            e.appendChild(entityRef);
	    		//printDocument();
	        }else{
	        	String id = subjectEntityNode.getAttributes().getNamedItem("id").getNodeValue();
	        	System.out.println("Entity not null: "+subjectClass+" Id: "+id);
	        	
	        	entityRef.setAttribute("ref", id);
	            e.appendChild(entityRef);
	        }
			
			String axiomType="";
			
			if (axiom.charAt(0)!='<'){
				axiomType= axiom.substring(0,axiom.indexOf("(") );
				
				//system.out.println("axiom type: "+axiomType);
				String innerAxiom = axiom.substring(axiom.indexOf("(")+1,axiom.lastIndexOf(")"));
				//system.out.println("inner axiom: "+innerAxiom);
				
				Element element = analyse(subject,axiomType,innerAxiom);
				e.appendChild(element);
				
			}else{ //Then its a class not an inner axiom
				subjectClass = axiom.substring(axiom.indexOf("#")+1,axiom.length()-1);
				subjectEntityNode = elementExists(subjectClass,doc,"entity");
				entityRef=doc.createElement("EntityRef");
				
				if ( subjectEntityNode==null) {
		            Entity subjectEntity = new Entity(subjectClass);
		            doc = subjectEntity.createEntity();
		            
		            entityRef.setAttribute("ref", subjectEntity.getId());
		            e.appendChild(entityRef);
		    		//printDocument();
		        }else{
		        	String id = subjectEntityNode.getAttributes().getNamedItem("id").getNodeValue();
		        	System.out.println("Entity not null: "+subjectClass+" Id: "+id);
		        	
		        	entityRef.setAttribute("ref", id);
		            e.appendChild(entityRef);
		        }
			}
			
		}else{
			//first axiom
			String axiom=checkAxioms(subject,e,values);
			System.out.println("******* First round axiom: "+axiom);
			
			//loop:
			while (!(axiom=checkAxioms(subject,e,axiom)).equals("")){

				if (axiom.charAt(0)=='<'){
					
					String subjectClass = axiom.substring(axiom.indexOf("#")+1,axiom.length()-1);
					System.out.println("subjectClass2: "+subjectClass);
					
					Node subjectEntityNode = elementExists(subjectClass,doc,"entity");
					Element entityRef=doc.createElement("EntityRef");
					
					if ( subjectEntityNode==null) {
			            Entity subjectEntity = new Entity(subjectClass);
			            doc = subjectEntity.createEntity();
			            
			            entityRef.setAttribute("ref", subjectEntity.getId());
			            e.appendChild(entityRef);
			    		//printDocument();
			        }else{
			        	String id = subjectEntityNode.getAttributes().getNamedItem("id").getNodeValue();
			        	System.out.println("Entity not null: "+subjectClass+" Id: "+id);
			        	
			        	entityRef.setAttribute("ref", id);
			            e.appendChild(entityRef);
			        }
					
				}
				//break loop;
			}
			
		}
		
		return e;
	}
	
	public Node elementExists(String name, Document doc, String tag) {
	    boolean exists=false;
	    Node node=null;
	    NodeList nl;
	    nl = doc.getElementsByTagName(tag);
	    System.out.println("number of "+tag+": "+nl.getLength());
	    if (nl.getLength() > 0) {
	    	for (int i=0;i<nl.getLength();i++){
	    		if (hasAttribute(nl.item(i),name)){
	    			node=nl.item(i);
	    			exists=true;
	    		}
	    	}
	    }
	    return node;
	}
	
	public static boolean hasAttribute(Node element, String value) {
	    NamedNodeMap attributes = element.getAttributes();
	    for (int i = 0; i < attributes.getLength(); i++) {
	        Node node = attributes.item(i);
	        if (value.equals(node.getNodeValue())) {
	            return true;
	        }
	    }
	    return false;
	}
	
	public void printDocument() throws Exception{
		// write the content into xml file
		TransformerFactory transformerFactory = TransformerFactory.newInstance();
		Transformer transformer = transformerFactory.newTransformer();
		
		transformer.setOutputProperty(OutputKeys.INDENT, "yes");
		transformer.setOutputProperty("{http://xml.apache.org/xslt}indent-amount", "2");
		
		DOMSource source = new DOMSource(doc);
		// Output to console for testing
		StreamResult result = new StreamResult(new StringWriter());
		transformer.transform(source, result);
		
		String xmlString = result.getWriter().toString();
		System.out.println(xmlString);
	}

}
