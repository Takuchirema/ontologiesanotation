import javax.xml.parsers.*;
import javax.xml.transform.*;
import javax.xml.transform.dom.*;
import javax.xml.transform.stream.*;
import org.xml.sax.*;
import org.w3c.dom.*;

public class Relationship {
	
	private String name;
	private String type;
	private String nAry="2";
	private String id;
	
	public Relationship(String name,String type){
		this.name=name;
		this.type=type;
	}
	
	public Document createEntity(Document doc) throws Exception{
		
		id=Test.getElementId();
		Test.incElementId();
		
		//get the root element
        Element rootElement = doc.getDocumentElement();
        
        Element e=doc.createElement("Relationship");
        e.setAttribute("name", name);
        e.setAttribute("type", type);
        e.setAttribute("nary", nAry);
        e.setAttribute("id", id);
        rootElement.appendChild(e);
        
        
        //create the roles
        Role role1=new Role(name);
        role1.createEntity(doc);
        
        Role role2=new Role(name);
        role2.createEntity(doc);
        
        //reference the roles
        Element ref1=doc.createElement("role");
        ref1.setAttribute("ref", role1.getId());
        
        Element ref2=doc.createElement("role");
        ref2.setAttribute("ref", role2.getId());
        
        e.appendChild(ref1);
        e.appendChild(ref2);
        return doc;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String getnAry() {
		return nAry;
	}

	public void setnAry(String nAry) {
		this.nAry = nAry;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}
	
	
}
