import org.w3c.dom.Document;
import org.w3c.dom.Element;


public class Role {
	
	private String name;
	private String id;
	private String tense="";
	private String roleCase="";
	private String hasPre="";
	private String rolePre="";
	
	public Role(String name){
		this.name=name;
	}
	
	public Document createEntity(Document doc) throws Exception{
		//get the root element
        Element rootElement = doc.getDocumentElement();
        
        Element e=doc.createElement("role");
        e.setAttribute("name", name);
        e.setAttribute("id", Test.getElementId());
        e.setAttribute("tense", tense);
        e.setAttribute("case", roleCase);
        e.setAttribute("hasPRE", "false");
        e.setAttribute("PRE", tense);
        rootElement.appendChild(e);
		
        return doc;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getTense() {
		return tense;
	}

	public void setTense(String tense) {
		this.tense = tense;
	}

	public String getRoleCase() {
		return roleCase;
	}

	public void setRoleCase(String roleCale) {
		this.roleCase = roleCale;
	}

	public String getHasPre() {
		return hasPre;
	}

	public void setHasPre(String hasPre) {
		this.hasPre = hasPre;
	}

	public String getRolePre() {
		return rolePre;
	}

	public void setRolePre(String rolePre) {
		this.rolePre = rolePre;
	}
	

}
